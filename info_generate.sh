#!/bin/sh

for f in $(ls -v -d /root/HashingClient/database/*);
 do
    if [ $f != "/root/HashingClient/database/now" ] && [ $f != "/root/HashingClient/database/name.txt" ] && [ $f != "/root/HashingClient/database/parentIp.txt" ] && [ $f != "/root/HashingClient/database/now.txt" ]; then
    [ -d $f ]
    cd "$f"
    echo $( basename "$f" ) >> /root/HashingClient/blockchain_info.txt
    cat "hash.txt" >> /root/HashingClient/blockchain_info.txt
    cat "timeStamp.txt" >> /root/HashingClient/blockchain_info.txt
    for n in *;
     do
        if [ $n != "hash.txt" ] && [ $n != "timeStamp.txt" ]; 
        then echo $( basename "$n" ) >> /root/HashingClient/blockchain_info.txt
        fi
     done
    echo " " >> /root/HashingClient/blockchain_info.txt
    fi
done
