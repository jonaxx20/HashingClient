#!/bin/sh
IP1=$1
IP2=$2
HASH=$3
ssh -o StrictHostKeyChecking=no -o "UserKnownHostsFile /dev/null" root@$IP1 " cd /HashingClient/database/now; ls > ./temp/block.txt; NOW=$(head -n 1 block.txt); rm -r temp; COPY=$((NOW-1))"
scp -3pr -o StrictHostKeyChecking=no -o "UserKnownHostsFile /dev/null" root@$IP1:/root/HashingClient/database/$COPY/. root@$IP2:/root/database/
ssh -o StrictHostKeyChecking=no -o "UserKnownHostsFile /dev/null" root@$IP2 "echo '"$HASH"' > /root/database/now.txt; rm -r /root/database/now"
